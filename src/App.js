// import logo from './logo.svg';
import './App.css';
import React from "react"
import * as Longan from "gs-longan"
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      viewer: null,
      viewModel: null
    };
    this.uploadGslFileChange = this.uploadGslFileChange.bind(this);
  }
  componentDidMount() {
    Longan.LonganLoad.import("js/longan/longan.js");
    Longan.LonganLoad.onReady().then(() => {
      let viewer = null
      let viewModel = null
      viewer = new Longan.Viewer("viewer");
      viewer.name = "main";
      viewer.backgroundColor = [237, 236, 237];

      viewer.sceneSegment.renderingOptions.setCullingMaximumExtent(10);
      viewer.operatorManager.add(new Longan.CameraMouse());
      viewModel = new Longan.ViewModel();
      viewModel.viewer = viewer;

      this.setState({ viewer: viewer });
      this.setState({ viewModel: viewModel });
    });
  }
  uploadGslFileChange() {
    console.log(this.state.viewer)
    let viewer = this.state.viewer
    let viewModel = this.state.viewModel
    let file = document.getElementById("uploadGslFile").files[0];
    let reader = new FileReader();
    reader.readAsArrayBuffer(file);
    reader.onload = (e) => {
      let data = e.target.result;

      let fileSegment = viewer.sceneSegment.fromStream(data);
      fileSegment.computeBoundingBox();
      viewer.fitWorld();
      viewer.updateDisplay();
      console.log(viewer, viewModel, e);
    };
  }
  render() {
    return (
      <div className="App">
        <input
          type="file"
          id="uploadGslFile"
          accept=".gsl, .gsl.sz"
          onChange={this.uploadGslFileChange}
        />
        <div id="viewer"></div>
      </div>
    );
  }
}

export default App;
